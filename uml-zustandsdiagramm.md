## UML-Zustandsdiagramm

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-zustand</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Mit UML-Zustandsdiagrammen lassen sich alle System modellieren, für die unter variierenden Randbedingungen unterschiedliches Verhalten anbieten oder ausführen. Das trifft auf User-Interfaces ebenso zu wie auf Embedded Systems: allen ist gemein, dass unterschiedliche Zustände jeweils besonderes Verhalten haben - und es definierte Zustandsübergänge mit Auslösern, Bedingungen und Übergangsverhalten gibt._

Dieser Artikel ist Teil einer Artikelreihe über UML-Diagramme: [Klassendiagramm](https://oer-informatik.de/uml-klassendiagramm) / [Sequenzdiagramm](https://oer-informatik.de/uml-sequenzdiagramm) / [Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm) / [Zustandsdiagramm (State)](https://oer-informatik.de/uml-zustandsdiagramm) / [Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase). 

Zu Zustandsdiagrammen gibt es weiterhin Artikel zur [Erstellung mit dem Programm PlantUML](https://oer-informatik.de/uml-zustandsdiagramm-plantuml), zur [Modellierung einer GUI mithilfe von Zustandsdiagrammen](https://oer-informatik.de/geschaeftsprozess-ui-states-entwerfen) und zu [Zustandsdiagramm-Übungsaufgaben](https://oer-informatik.de/uml-zustand-uebung-session).

## Was beschreibt das UML-Zustandsdiagramm?

Das UML-Zustandsdiagramm modelliert ein System unter folgenden Gesichtspunkten:

* die unterschiedlichen Zustände, die ein Objekt/System im Verlauf der Lebenszeit einnehmen kann,

* welche Ereignisse den Zustandswechsel hervorrufen und

* welche Aktionen in den jeweiligen Zuständen möglich sind.

Als Verhaltensdiagramm stehen die dynamischen Vorgänge eines Systems im Fokus.

## Grundelemente des Zustandsdiagramms

Zustandsdiagramme bestehen im Wesentlichen aus Knoten und gerichteten Kanten, im Wesentlichen:

* Startzustand,
* Endzustand,
* Transitionen und
* Zustände

![Ein einfaches UML-Zustandsdiagramm mit den Zuständen "hungrig" und "satt"](plantuml/00_zustand-basic.png)

## Welche Einsatzgebiete haben UML-Zustandsdiagramme?

Zustandsdiagramme werden v.a. in Planungs- und in der Testphase des Softwareentwicklungsprozesses genutzt, um graphisch die Frage zu modellieren:

> Wie verhält sich mein System in einem bestimmten Zustand bei Eintritt bestimmter Ereignisse?

Wie bei allen UML-Diagrammen sollte auch hier im Fokus stehen: _Wem_ möchte ich mit meinem Zustandsdiagramm _was_ mitteilen? Adressat und Nachricht müssen immer berücksichtigt werden.

Detailreichtum, Komplexität der Notationselemente und  Namensgebung müssen entsprechend dem Adressaten und der Intention angepasst werden. Dies kann auch von Phase zu Phase variieren:

- In der _Analysephase_ werden die Zustandsabfolgen und -übergänge der Geschäftsprozesse dargestellt und  einzelne Szenarien der Anwendungsfälle (oder übergeordnete Zustandsfolgen) präzisiert. Zustandsdiagramme eignen sich besonders gut, um ereignisgesteuerte Abläufe wie etwa die Navigation in einer (graphischen) Benutzeroberfläche ([G]UI) zu modellieren. Die Übergänge der einzelnen Masken, Fenster u.ä. kann im Dialog mit anderen Projektbeteiligten, dem Auftraggeber, der Fachdomäne erarbeitet werden.

- In der _Entwurfsphase_ kann das Zustandsdiagramm dabei helfen, Sonderfälle in der Ereignisabfolge systematisch zu strukturieren: Wann sollen die Objekte meines Systems anders reagieren? Wie kann dies modelliert - und später implementiert - werden?

- In der _Testphase_ kann an Hand der Zustände und herausgearbeiteten Transitionen ein Grundgerüst an Testfällen erstellt werden. Es ist in der Regel ratsam, jeden Zustand und jede Transition mit mindestens einem Testfall abzudecken.


## Bestandteile eines UML-Zustandsdiagramms

Grundsätzlich besteht ein Zustandsdiagramm aus Knoten (Zuständen) und gerichteten Kanten (Transitionen):

### Startzustand

Einstiegspunkt des Diagramms ist der Startzustand (_initial state_). Er legt fest, welchen Zustand das modellierte System am Beginn einnimmt. Der Startzustand wird mit einem ausgefüllten Kreis notiert.

![Der Startzustand](plantuml/00_zustand-start.png)

Da das System sich nie im Startzustand selbst befindet spricht man hier von einem _Pseudozustand_. An dem Übergang (Transition) vom Startzustand zum ersten regulären Zustand dürfen keine Bedingungen (_guards_) notiert werden - sofern dieser Zustandsübergang mit einer Instanziierung (Objekterzeugung) zusammenfällt könnte jedoch der _event_ notiert werden, der die Objekterzeugung triggert.

Ein modelliertes System kann auf jeder Ebene nur einen Startzustand haben.

### Zustände

Ein einfacher Zustand (_simple state_) fasst Situationen zusammen, in denen sich das modellierte Objekt die selben Aktionen und Zustandsübergänge ausführen kann. Zustände werden in Rechtecken mit abgerundeten Ecken notiert, in denen mindestens der Name des Zustands steht.

![Der Zustand hungrig](plantuml/00_zustand-state.png)

In der unteren Hälfte eines Zustands können (optional) innere Aktionen notiert werden. Innere Aktionen sind alle Aktionen, die keinen Zustandswechsel hervorrufen. Die Trennlinie kann weggelassen werden, wenn nur der Zustandsname angegeben wird.

### Transitionen

Die gerichteten Kanten (Pfeile) des Zustandsdiagramms symbolisieren Zustandsübergänge (Transitionen).

![Die Zustände hungrig und satt](plantuml/00_zustand-transition.png)

Durch Ereignisse (_events_) werden Zustandsübergänge (_transitions_) ausgelöst. Transitionen zeigen von einem Quell- zu einem Zielzustand.

An Transitionen können drei Eigenschaften notiert werden: `event [guard] /effect`:

![Die Zustände hungrig und satt](plantuml/00_zustand-transition-mit-event.png)

* _Events_ sind die Trigger, die den Zustandsübergang auslösen (im Beispiel oben: `essen`).

* _Guards_ sind die Bedingungen, die geprüft werden, nachdem ein _events_ eingetreten ist. Trifft ein _guards_ zu wird der Zustandübergang durchgeführt. Diese Bedingungen werden in eckigen Klammern nach den _events_ notiert (im Beispiel: `[wenn Essen da]`).

* Effekte (_effects_) werden aussgeführt, während der Zustandsübergang stattfindet. Nach Abschluss des Effekts ist der Zustandsübergang erfolgt. Effekte werden an der Kante nach einem Schrägstrich notiert (im Beispiel: `/schmatzen()`). Häufig handelt es sich hierbei um Methoden oder Funktionen des modellierten Systems, diese werden mit runden Klammern abgeschlossen.

### Endzustand

Der Endzustand (_final state_) ist ein regulärer Zustand, in dem sich das System am Ende befindet, aus dem es aber nicht mehr in andere Zustände innerhalb der Modellierung wechseln kann.

![Der Endzustand](plantuml/00_zustand-stop.png)

Im Gegensatz zu Startzuständen gelten Endzustände als reguläre Zustände. Daraus folgt: es kann mehrere Endzustände oder mehrere Transitionen zu Endzuständen geben und an diesen können auch  _events_, _guards_ oder _effects_ notiert werden.

### Entscheidungsknoten: _choices_ und _junctions_

#### Statische Verzweigungen mit _junctions_

Die einfachste Art, eine Verzweigung zu modellieren, ist es, mehrere Transaktionen zu notieren. Wird der _event_ aktiviert, so wird anschließend der _guard_ geprüft, dann der _effect_ ausgelöst und der Zustandsübergang ausgeführt:

![Der Entscheidungsknoten](plantuml/00_zustand-junction1.png)

Beispiel: Vor dem Zustandsübergang befindet sich das System in `State1`, und es gilt `a==1`. Wenn `trigger` eintritt wird zunächst geprüft, ob `a==1` - das trifft zu, also wird die Transition Richtung `State2` ausgeführt und dabei a um eins erhöht.

Da sich in diesem Beispiel _event_ und _effect_ für die unterschiedlichen Transitionen nicht unterscheidet, gibt es eine Kurzform, die  diese beiden Transaktionen zusammenfasst: es wird ein _junction_-Pseudostate durch einen Kreis notiert.

 ![Der Entscheidungsknoten](plantuml/00_zustand-junction-orginal.png)

Wichtig hierbei ist: der _guard_ der beiden ausgehenden Kanten wird geprüft _bevor_ (!) der _effect_ (hier `a++`) ausgelöst wird. Man spricht hier daher von einem _static conditional branch_ also einer statischen bedingten Verzweigung. _Junctions_ stellen nur eine Kurzschreibweise für mehrere einzelne Transaktionen dar.

#### Dynamsiche Verzweigungen mit _choices_

Im Gegensatz zu den statischen _junctions_ werten dynamische Entscheidungsknoten (_choices_) die _guards_ erst beim Erreichen des Entscheidungsknoten aus. _Choices_ werden durch eine i.d.R. leere Raute dargestellt. Vor der Wahl der ausgehenden Transition wurde also der _effect_ bereits ausgeführt. In unserem Beispiel sähe das so aus:

![Der Dynamische Verzweigungen - der choice-Entscheidungsknoten](plantuml/00_zustand-choice.png)

Vor dem Zustandsübergang befindet sich das System in `State1`, und es gilt `a==1`. Wenn `trigger` eintritt wird zunächst der _effect_ `a++`  ausgeführt (es gilt also jetzt `a==2`). Erst jetzt werden die _guards_ überprüft: nur der `else`-Zweig wird als `true` ausgewertet, die Transition findet also in Richtung `State3` statt. Wir erreichen also in diesem Beispiel mit _choices_ einen anderen Zustand als bei _junctions_!

Da im Fall einer _choice_ der Quellzustand bereits verlassen wurde, wenn der _guard_ ausgewertet wird, muss sichergestellt sein, dass für jeden möglichen Fall mindestens ein _guard_ erfüllt ist, da sich das System sonst im Pseudozustand befindet - also nicht korrekt modelliert wurde. Am einfachsten erreicht man dies, in dem an einer der ausgehenden Transaktionen als _guard_ `else` notiert wird.


### Innere Aktionen

Für Zustände kann ein inneres Verhalten (_internal behavior_) sowie innere Transitionen modelliert werden.

![Die inneren Aktionen entry/do/exit sowie eigene](plantuml/00_zustand-innereAktionen.png)

Es gibt drei Arten von **innerem Verhalten**:

- die `entry`-Aktivitäten werden immer dann ausgeführt, wenn der Zustand über eine externe Transition betreten wird.

- die `exit`-Aktivitäten werden immer dann ausgeführt, wenn der Zustand über eine externe Transition verlassen wird.

- `do`-Aktivitäten werden nach den `entry`-Aktivitäten ausgeführt. Sofern Sie abgeschlossen werden, triggern Sie einen `completion event`. `Completion events` wiederum triggern Transitionen ohne eigene Trigger, deren _guard_ als `wahr` ausgewertet wird. Das passiert genau einmal, am Ende der `do`-Aktivität. (Im obigen Beispiel: wenn im Zustand `wach` _arbeiten_ beendet wurde wird ein `completion event` getriggert und geprüft, ob der _guard_ `[Müdigkeit>Wille]` zutrifft. Falls dem so ist wird der Zustandsübergang eingeleitet).


Im Zustand "wach" stellen _Kaffee trinken_, _arbeiten_ und _Zähne putzen_ inneres Verhalten dar.

Neben diesen drei Arten inneren Verhaltens gibt es noch **innere Transitionen**: diese entsprechen den externen Transitionen mit dem Unterschied, dass sie weder `exit` noch `entry`-Verhalten triggern, noch einen Zustandswechsel hervorrufen.

Der Trigger "Vollmond" löst bei Werwölfen zwar keinen Zustandswechsel aus (der Zustand bleibt "schlafend"), triggert aber eine innere Aktion (Schlafwandeln). Weder hört das System auf zu `träumen`, noch würden in diesem Fall die Augen geöffnet und wieder geschlossen.

Im Gegensatz dazu werden bei externen Transitionen immer die modellierten `entry`- und `exit`-Verhalten ausgeführt. Das gilt auch, wenn die externe Transition wieder zum Ausgangszustand zurückführt. Wenn man im Schlaf aufgeschreckt wird, würde im dem obigen Modell:

1. der Trigger "aufgeschreckt" registriert,
2. geprüft, ob der _guard_ zutrifft (`zeit<weckzeit`),
3. das `do`-Verhalten unterbrechen und `exit`-Verhalten "Augen auf" ausgeführt,
4. der `effect` der Transition ausgeführt: "weiterschlafen",
5. das `entry`-Verhalten "Augen zu" ausgeführt und
6. das `do`-Verhalten starten: `träumen`.



## Zusammengesetzte Zustände (composite state)

Zusammengesetze Zustände bilden innere Subzustände ab. Es bestehen zwei Möglichkeiten, diese mit UML zu modellieren:

In einem gemeinsamen Diagramm werden die zusammengesetzten Zustände und ihre Subzustände dargestellt:

![Zusammengesetzte Zustände und Subzustände](plantuml/00_zustand-subzustaende.png)

Die beiden Ebenen (zusammengesetzter Zustand / Subzustände) können in gesonderten Diagrammen dargestellt werden. Hierbei wird im zusammengesetzten Zustand durch ein Symbol notiert, dass sich dieser aus Subzuständen zusammensetzt. Das Symbol soll zwei Zustände und eine Transition stilisiert darstellen.

![Zusammengesetzter Zustand in Darstellung ohne Subzustände](plantuml/00_zustand-subzustaende2.png)

Die Subzustände selbst würden in diesem Fall in einem eigenen Zustandsdiagramm dargestellt. Das Diagramm wir überschrieben mit "stm" für _State Machine_ mit dem Namen des zusammengesetzten Zustands:

![Die Subzustände eines zusammengesetzten Zustands als eigenes Diagramm](plantuml/00_zustand-subzustaende3.png)

Es muss für jede Ebene gesondert erfasst werden, in welchem Zustand sich das System zu Beginn befindet bzw. aus welchem Zustand heraus das System beendet wird:

![Start- und Endzustand für zusammengesetzte Zustände](plantuml/00_zustand-subzustaende4.png)

Dabei können Subzustände direkt mit Transitionen erreicht werde: im Beispiel unten
ist der Startzustand des Systems immer `wach.hungrig`, den Zustand `schlafend` kann man nur aus den Subzuständen `wach.satt` und `wach.überfressen` erreichen. Wenn man vom Zustand `schlafend` per Wecker geweckt wird, nimmt man wieder den Subzustand von `wach` ein, den man vor dem Einschlafen hatte (also `satt` oder `überfressen`). Das wird im UML-Zustandsdiagramm mit dem  _History_ Symbol (H) gekennzeichnet. Sofern der betreffende zusammengesetzte Zustand bislang nicht betreten wurde ist das System in dem Subzustand, auf den die ausgehende Transition des History-Pseudozustands zeigt (im Beispiel also `hungrig`). Diese Fall nennt sich "flache Historie", da nur eine Ebene berücksichtigt wird.

![Flache Historie eines zusammengesetzten Zustands](plantuml/00_zustand-subzustaende5.png)

Eine Erweiterung dazu stellt die "tiefe Historie" dar. Hierbei wird der Subzustand nicht nur auf der ersten Ebene, sondern so tief wie möglich wiederhergestellt. Gekennzeichnet wird dies mit dem (H\*) Symbol. Wenn der Wecker klingelt und der letzte Subzustand von `wach` war `hungrig`, dann würde das System auch unmittelbar wieder in den vorigen Subzustand von `hungrig`  wechseln (also `vegetarisch` oder `omnivor`):

![tiefe Historie](plantuml/00_zustand-subzustaende6.png)

Der Ausgangszustand ist - wie die beiden Transitionen von den (H)- bzw. (H\*) Pseudozuständen zeigen - hungrig und vegetarisch.


## Regionen in zusammengesetzten Zuständen

Eigentlich ist auf jeder Ebene eines UML-Zustandsdiagramms immer nur ein Zustand aktiv.
Es können aber bei Subzuständen Regionen definiert werden, in denen jeweils einer der verzeichneten Zustände aktiv ist:


![tiefe Historie](plantuml/00_zustand-regions.png)


## Weitere Literatur zu UML-Zustandsdiagrammen

- **als Primärquelle**: die UML Spezifikation der Object Management Group
Definition des Standards, jedoch nicht für Endnutzer aufbereite. Die für das Zustandsdiagramm relevanten Abschnitte finden sich unter dem Kapitel _14.2 Behavior StateMachine_ (S. 305 bzw. PDF_Seite 347). Die Notationselemente finden sich ab S. 708 (im PDF S. 750) [https://www.omg.org/spec/UML](https://www.omg.org/spec/UML)

- **für den Einstieg**: Martina Siedl, Marion Brandsteidl, Christian Huemer, Gerti Kappel: UML@Classroom, dpunkt Verlag, Heidelberg 2012
Gut zu lesende Einführung in die wichtigsten UML-Diagramme, Empfehlung für den Einstieg - (nicht auf Stand der neusten UML-Version, das betrifft aber nur Detailfragen)

- **für Lesefaule**: Die Vorlesungsreihe der Technischen Uni Wien (zu UML@Classroom) kann hier angeschaut werden (Videos, Folien): [http://www.uml.ac.at/de/lernen](http://www.uml.ac.at/de/lernen)

- **als Nachschlagewerk**: Christoph Kecher, Alexander Salvanos: UML 2.5 – Das umfassende Handbuch, Rheinwerk Bonn 2015, ISBN 978-3-8362-2977-7, ein sehr umfangreiches Nachschlagewerk zu allen UML-Diagrammen

- **als Cheatsheet**: die Notationsübersicht von oose.de: [https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf](https://www.oose.de/wp-content/uploads/2012/05/UML-Notations%C3%BCbersicht-2.5.pdf)

- **UML und Software Engineering**: Chris Rupp, Stefan Queins & die SOPHISTen: UML2 glasklar, Hanser Verlag, München 2012; ISBN 978-3-446-43057-0 Schwerpunkt: Einbindung von UML-Diagrammen in den Softwareentwicklungszyklus

- **Zur Zertifizierungs-Vorbereitung**: M. Chonoles: OCUP 2 Certification Guide , Morgan Kaufmann Verlag, Cambridge 2018, ISBN 978-0-12-809640-6
    Informationen zur Zertifizierung nach OMG Certified UML Professional 2™ (OCUP 2™): Foundation Level

## Software zur Erzeugung von UML-Zustandsdiagrammen

- [PlantUML](http://www.plantuml.com): Deklaratives UML-Tool, das in vielen Entwicklungsumgebungen integriert ist. auch als WebEditor verfügbar. Die obigen Diagramme wurden damit erzeugt - [Eine detaillierte Anleitung für plantUml mit den Quelltexten findet sich hier](https://oer-informatik.gitlab.io/uml/umlzustand/uml-zustandsdiagramm-plantuml.html)

- [WhiteStarUML](https://sourceforge.net/projects/whitestaruml/) (Relativ umfangreiches Tool, viele UML-Diagramme, mit Code-Generierung für Java, C, C# und Vorlagen für Entwurfsmuster)

- [Draw.io](http://www.draw.io): Online-Tool für Flowcharts usw - aber eben auch UML


## _Quellen und offene Ressourcen (OER)_

Die Ursprungstexte (als Markdown), Grafiken und zugrunde liegende Diagrammquelltexte finden sich (soweit möglich in weiterbearbeitbarer Form) in folgendem git-Repository:

[https://gitlab.com/oer-informatik/uml/umlzustand](https://gitlab.com/oer-informatik/uml/umlzustand).

Sofern nicht explizit anderweitig angegeben sind sie zur Nutzung als Open Education Resource (OER) unter Namensnennung (H. Stein, oer-informatik.de) freigegeben gemäß der [Creative Commons Namensnennung 4.0 International Lizenz (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/deed.de).

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/deed.de)
