## Übungsaufgabe zum UML-Zustandsdiagramm

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-zustand-uebung-session</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/deck/@oerinformatik/113726239470744032</span>

> **tl/dr;** _(ca. 30 min Bearbeitungszeit): Übungsaufgabe zu UML-Zustandsdiagrammen: Es soll eine Art Genehmigungsprozess modelliert werden mit Zuständen, Transitionen, zusammengesetzen Zuständen und inneren Aktivitäten._

Hintergrundinformationen finden sich in diesem [Grundlagenartikel zum UML-Zustandsdiagramm](https://oer-informatik.de/uml-zustandsdiagramm).

### Hintergrundinformation zur Aufgabe

Diese Modellierung erfolgt im Zusammenhang mit der Erstellung einer WebApp zur Organisation eines Mitmach-Kongresses: alle Kongress-Teilnehmer können per Online-Formular Veranstaltungsideen erstellen, bearbeiten und einreichen, ein Organisationsteam entscheidet darüber, welche Veranstaltungen am Ende durchgeführt werden. Veranstaltungen können Vorträge, Tutorials, Diskussionen usw. sein - sie werden im Folgenden vereinfacht als _Session_ bezeichnet.

### Aufgabenstellung

Die Vorschläge für _Sessions_ werden mithilfe einer App gesammelt. Dabei können _Sessions_ folgende Stadien durchlaufen:

- Zunächst gilt eine Session als _entworfen_, solange sie bearbeitet wird. Ist die Bearbeitung abgeschlossen, wird der Text gespeichert und sie gilt danach als _ausformuliert_.

- Sobald eine Session _ausformuliert_ ist, wird die Prüfung durch den Autor vorbereitet. Eine Session wird unentwegt überprüft, solange sie _ausformuliert_ ist. Die Prüfergebnisse werden gespeichert, sobald die Session nicht mehr _ausformuliert_ ist. Es gibt nur drei Ereignisse, die dafür sorgen, dass eine Session nicht mehr _ausformuliert_ ist: 

    - Der Autor / die Autorin hat Korrekturen: Dabei werden die Prüfergebnisse erst gespeichert und dann eine neue Prüfung vorbereitet. Die Session ist nach den Korrekturen wieder _ausformuliert_.

    - Es gibt Ergänzungen des Autors / der Autorin: hierdurch gilt die Session wieder als eine _entworfene Session_: die Prüfergebnisse werden gespeichert und die Session wird wieder bearbeitet, bis die Bearbeitung abgeschlossen ist (siehe oben).
    
    - Wenn das Formular abgesendet wird, gilt eine _ausformulierte_ Session als _eingereicht_, so fern alle Felder ausgefüllt sind. Sind nicht alle Felder ausgefüllt, ist die Session nach dem Absenden des Formulars wieder _ausformuliert_ (sprich: Formular wird abgesendet, Vollständigkeit wird geprüft, die Prüfergebnisse werden gespeichert - bei fehlenden Formulardaten wird dann die Prüfung wieder vorbereitet, siehe oben).

- Eine _entworfene_ und _ausformulierte_ Sessions können in den Papierkorb gelegt werden. Aus dem Papierkorb können sie entweder endgültig gelöscht oder wiederhergestellt werden. Wiederhergestellte Sessions gelten entweder als _entworfen_ oder als _ausformuliert_, je nach dem, welches Stadium sie vor dem Löschen zuletzt hatten.

- Eine _eingereichte Session_ ist durch Zustimmung des Orgateams _genehmigt_. Während der Genehmigung wird eine Bestätigungsmail an den Autor / die Autorin gesendet.

- Eine _eingereichte Session_ wird durch Ablehnung des Orgateams wieder eine _ausformulierte Session_. Während der Ablehnung wird eine Bestätigungsmail an den Autor / die Autorin gesendet.

- Eine _genehmigte Session_ wird _durchgeführt_, sobald der Zeitpunkt der Session startet. Bei einer _durchgeführten Session_ werden zu Beginn alle nötigen Dokumente bereitgestellt. Danach wird die Session durchgeführt. Sobald die Durchführung beendet ist, wird die Session evaluiert.

- Eine _durchgeführte_ Session wird archiviert, bevor alles beendet wird.

- Eine _eingereichte_ oder _genehmigte_ Session kann durch den Autor / die Autorin zurückgezogen (in den Papierkorb gelegt). Wird diese Session wiederhergestellt, gilt sie als _ausformulierte_ Session.

- Eine _durchgeführte_ Session kann nicht gelöscht werden.

Erstelle für die unterschiedlichen Zustände einer Session ein UML-Zustandsdiagramm mit allen genannten Informationen, aus dem die Zustandsübergänge, Aktivitäten und Bedingungen wie oben genannt hervorgehen.

### Beispiellösung

Wer nach der Bearbeitung das eigene Ergebnis vergleichen will, der kann sich folgende Beispiellösung anschauen:

<button onclick="toggleAnswer('sessionbuchung')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="sessionbuchung">

Die modellierten Zustände könnten etwa so aussehen - wobei leider das Tool bei den zusammengesetzten Zuständen eckige Umrandungen (was Objekte/Klassen wären) statt runder Umrandungen gewählt.

![Darstellung Zustandsdiagramm zur Sessionplanung](plantuml/99_zustand-sessionplanung.png)

Hinweis zur Lösung:

Einige Eigenschaften lassen sich nur über Subzustände und Historie lösen: das Wiederherstellen der Session in den Zuständen _ausformuliert_ oder _entworfen_ ist anders nicht realisierbar.

Die Zustände _genehmigt_ und _eingereicht_ teilen sich die Transition "Session durch Autor zurückgezogen" - auch das lässt sich am einfachsten über einen zusammengesetzten Zustand realisieren.

Die Formulierungen sind so gewählt, dass klar zwischen _Triggern_, _Guards_ und _Effekten_ unterschieden werden kann, lediglich beim Übergang von "genehmigt" zu "durchgeführt" wäre auch eine zeitliche Triggerung denkbar.

Bei den inneren Aktivitäten ist zu beachten, dass die _do_ -Aktivitäten solange laufen, bis sie beendet sind - es sei denn, ein Trigger, der den Zustandsübergang einleitet, unterbricht sie. Bei der letzten Transition aus dem "durchgeführt"-Zustand wird der Zustandsautomat beendet, sobald die _do_ -Aktität abgeschlossen, die _exit_ -Aktivität beendet und der Transitionseffekt abgeschlossen ist.

Bepunktungsvorschlag gesamt 31P:

- 6 Zustände: 6x1P = 6P
- 7 innere Aktivitäten 7x1P = 7P
- 5 Transitionen mit Effekt (3x Mail, speichern, archivieren): 5P
- 1 Choice (mit zwei Abzweigen): 3P
- Transition mit Guard oder Zeit-Event (Zeitpunkt): 1P
- Papierkorb-Transitionen (legen, wiederherstellen, löschen - ohne zurückziehen, das ist oben schon drin): 3P
- Einfache Transitionen: Ergänzen, Korrektur, Start, Stop: 4P
- Subzustand mit Historie: 2P



</span>
