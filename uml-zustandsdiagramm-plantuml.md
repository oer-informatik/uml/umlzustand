## UML-Zustandsdiagramme mit PlantUML erstellen

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-zustandsdiagramm-plantuml</span>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Mithilfe des Tools PlantUML lassen sich textbasiert UML-Zustandsdiagramme erstellen._


Hintergrundinformationen finden sich in diesem [Grundlagenartikel zum UML-Zustandsdiagramm](https://oer-informatik.de/uml-zustandsdiagramm).


[PlantUML](https://plantuml.com/de/) ist ein vielseitig nutzbares Tool, um testbasiert versionierbare (UML-)Diagramme zu erstellen. Aus einfachen Quelltexten lassen sich so UML-Diagramme als PNG, ASCII-Art oder SVG erstellen. PlantUML ist als Webservice verfügbar (z.B. https://www.planttext.com/), in vielen IDEs (als Plugin) integriert und wird von einigen Markdown-Interpretern direkt umgewandelt. Allgemeine weiter Infos zu PlantUML finden sich hier: https://www.planttext.com/

Da die Anleitung zu UML-Diagrammen nicht immer den aktuellen UML-Standard im Blick haben, versuche ich hier praktische Tipps zu geben, wie standardkonforme UML-Zustandsdiagramme mit PlantUML erstellt werden können.

### Grundbausteine des UML-Zustandsdiagramms

UML-Zustandsdiagramme bestehen im Wesentlichen aus einem Startzustand, einzelnen Zuständen, einem Endzustand (optional) sowie Transitionen, die die Zustände verbinden. In PlantUML werden Zustände zunächst deklariet (`state Zustand1` oder bei längerem Namen mit Alias `state "Dargestellter Name des \nZustands 2"  as Zustand2`) und die verbindenden Transitionen in Zeilen mit stilisiertem ASCII-Pfeil beschrieben:

```plantuml
state hungrig
state "satt" as wohlgenährt
hungrig -> wohlgenährt
```

![Die Zustände hungrig und satt](plantuml/00_zustand-transition.png)

Start- und Endzustand werden in PlantUML mit `[*]` notiert. An Hand der Pfeilrichtung wird automatisch erkannt, ob es  mit sich um einen Start- oder einen Endzustand handelt:

 ```plantuml
 left to right direction
 [*] --> Zustand1
 Zustand1 --> [*]
 ```
![Der Pseudozustand "Start" und der Endzustand](plantuml/00_zustand-startstop.png)

### Transitionen

An den Transitionen können - wie in der UML üblich - Event (Trigger), Guard (Bedingung) und Effekt (Aktion während der Transition) notiert werden (in der Form: `startzustand -> zielzustand: Trigger [Guard] / Effekt`). In PlantUML sieht es wie folgt aus:


```plantuml
state hungrig
state satt

hungrig -right-> satt : essen [wenn Essen da] /schmatzen()
```
![Die Zustände hungrig und satt mit Trigger, Guard und Effekt an den Transitionen](plantuml/00_zustand-transition-mit-event.png)

### Innere Aktionen der Zustände

Die Zustände können mit inneren Aktionen versehen werden, sie werden mit Hilfe des Zustandsnamens und folgendem Doppelpunkt notiert - entweder nach dem Muster

`Event [Guard] / interneAktion`

oder entsprechend der drei internen Events: `entry`, `do`, `exit`:

```plantuml
state schlafend
schlafend : entry/Augen zu
schlafend : do/träumen
schlafend : exit/Augen auf
schlafend : Vollmond [Werwolf == true]/Schlafwandeln


state wach
wach : entry/Kaffee trinken
wach : do/arbeiten
wach : exit/Zähneputzen


wach -right-> schlafend : [Müdigkeit > Wille] /Schlafanzug anziehen
schlafend -left-> wach : Weckerklingeln [zeit>=weckzeit] /aufstehen
schlafend -left-> schlafend : aufschrecken [zeit<weckzeit] /weiterschlafen
```

![Innere Aktionen (entry, do, exit) in Zuständen](plantuml/00_zustand-innereAktionen.png)


### Verzweigungen (Choices)

Um dynamische Entscheidungen (Choices) in PlantUML einzubinden muss die Raute als Pseudozustand mit dem Stereotyp `<<choice>>` deklariert werden und danach wie jeder andere Zustand eingebunden werden:

```plantuml
state State1: entry/a=1

state entscheidung <<choice>>
state State2
state State3
State1 -> entscheidung : trigger /a++
entscheidung -> State2 :[a == 1]
entscheidung -> State3 :[else]

```

Wichtig: bei _Choices_ wird der _Guard_ erst nach dem Effekt ausgewertet. In folgendem  Beispiel wird a (in `State1`: $a = 1$) also erst hochgezählt ($a++$, also $a=2$) und dann der _Guard_ ausgewertet ($(a == 1) = false$) - es wird also immer die `[else]`-Transition genommen und `State3` erreicht.

![Innere Aktionen (entry, do, exit) in Zuständen](plantuml/00_zustand-choice.png)

### Kreuzungen (Junctions)

Sofern die Guards bereits vor den Effekten ausgewertet werden sollen (statische Entscheidungen / _junctions_), müssen im UML-Diagramm Knoten (ausgefüllte Kreise) notiert werden. In PlantUML ist das aber nicht möglich. Daher müssen diese zusammenhängenden Transitionen als einzelne Transitionen mit identischem Trigger und unterschiedlichen Guards modelliert werden:

```plantuml
state State1
State1 : entry/ a=1

state State2
state State3

State1 -right-> State2 : trigger[a == 1]/a++
State1 --right-> State3 :trigger[else]/a++
```

![Kreuzungen (Junctions) müssen in Plantuml als einzelne Transitionen modelliert werden](plantuml/00_zustand-junction1.png)


### Zusammengesetzte Zustände (_composed states_)

Mit Hilfe von geschweiften Klammern können _composed states_ beliebig verschachtelt werden:

```plantuml
state schlafend
state wach{
    state hungrig
    state satt
    hungrig -> satt : gegessen
    satt -> hungrig : verdaut
}

schlafend -> wach : Wecker
wach -> schlafend : einschlafen
```

![Composed States](plantuml/00_zustand-subzustaende.png)

Wenn Sie nur als Verweis referenziert werden sollen muss man mit PlantUML allerdings etwas zaubern, um das Symbol, das zwei Zustände zeigt in die Ecke zu schreiben (Workaround: Tab (Unicode #9) zur Einrückung und Unendlich-Symbol (Unicode #8734) zur Darstellung der Unterzustände):

```plantuml
@startuml
state "zusammengesetzter\nZustand"  as subState
subState: &#9;&#9;&#9;&#9;<b>&#8734;</b>
@enduml
```

![Composed States](plantuml/00_zustand-subzustaende2.png)


### Regionen in zusammengesetzten Zuständen

Mit Hilfe von zwei Bindestrichen lassen sich Regionen mit Unterzuständen definieren.


```plantuml
@startuml
left to right direction
state wach {
  [*] --> zufrieden
  zufrieden --> unzufrieden : Sonne
  unzufrieden --> zufrieden : Regen
  --
  [*] --> neugierig
  neugierig --> gelangweilt : warten
  gelangweilt --> neugierig : loslegen
}
@enduml
```

![Substates in Regionen aufgeteilt](plantuml/00_zustand-regions.png)


### Improvisationen für nicht vorhandene Notationselemente (History / Terminator)

Nutzt man die erweiteten PlantUML-Funktionen, so lassen sich auch Notationselemente wie der Terminator (Beenden des Zustandsautomaten) und die Speicherfunktion eines Unterzustands (History) abbilden:

![History über Sprites darstellen](plantuml/00_zustand-subzustaende6.png)

Über sprite lassen sich beliebige pngs als Base64url einfügen, die per
java -jar plantuml.jar -encodesprite 16z test.png
umgewandelt werden und per <$spriteName> angesprochen werden.
Der Terminator wird als sprite in einen Zustand geschrieben, dessen Ränder und Schrift per Stereotyp auf weiß gestellt sind.
Darüber hinaus wurden auch alle Notizenzettel auf weiß gestellt, so dass in diesen Boxen die Regionsnamen und das History-Objekt einigermaßen notationskonform dargestellt werden kann. Notizen sind so allerdings nicht mehr möglich.


```plantuml
@startuml
left to right direction
skinparam state {
  BackgroundColor<<terminate>> White
  FontColor<<terminate>> White
  BorderColor<<terminate>> White
}
skinparam  NoteBorderColor White
skinparam  NoteBackgroundColor White


skinparam stateShape start
skinparam shadowing false
sprite $history [30x30/16z] bTE54OKm50NHxM3xh_OBDfQTHoxEGQAyZLV2dfEmjIjzhP2iaApGBCluFDH7MUe9jWfIoevUsTVz_quredLkIKg77cpMOUGawpHYUxJFCsNE5m
sprite $terminator [30x30/16z] RSat0G0n5CMmp-3nHtjbygBqMDlEO_-8WJ4m5dOJmILoeH-nFx52h17ZK1oQGxCnDSR2s7ZaQWm4L_eO-neBm9An6993S0WEpIy

skinparam  NoteBorderColor White
skinparam  NoteBackgroundColor White
state wach {
  note left of zufrieden : [glücklich]
  [*] --> zufrieden
  zufrieden --> unzufrieden : Sonne
  unzufrieden --> zufrieden : Regen
  note right of unzufrieden : <$history>
  unzufrieden --> [*] : einschlafen
  --
  note left of neugierig : [interessiert]
  [*] --> neugierig
  neugierig --> gelangweilt : warten
  gelangweilt --> neugierig : loslegen
  gelangweilt -->end<<terminate>> :totlangweilen
 end : <$terminator>
}
@enduml
```

### Geerbte Zustände

Auch bei Zuständen gibt es eine Erbfolge - von Elternzuständen geerbte Zustände werden mit gestrichelter Linie dargestelle - in PlantUML folgendermaßen:

```plantuml
@startuml
state gererbterZustand ##[dashed] {
}
@enduml
```


### plantUML-Webservice zur Diagrammerzeugung nutzen

PlantUML bietet einen Webservice, der Diagramme unmittelbar online rendert.

* Variante 1: **Codierter Quelltext**: PlantUML kodiert den Quelltext, um ihn so über den Webservice zugänglich (und bearbeitbar) zu machen. Die URL ist in diesem Fall wie folgt aufgebaut:
 [http://www.plantuml.com/plantuml/AUSGABEFORMAT/CODIERTERQUELLTEXT](http://www.plantuml.com/plantuml/uml/fPA_JWCn3CPdyXHM5yfmBr09gVy40rMNWX28nStvxg9BdCfnE07YtScfAdHWWcon_VtysSayAOhcu4tg7HzGCC2Q6inURoBh5WF1P9Ejgn5so0dktmuqY5EIYJdJF2HQOQ8F0-KiezGag-YZm1gbttbKMlfCnopQlfMOkJvM35sXfH1xCfzdn6tKF-4shktqYRoFG-6T0HTMe_pRu3bG90w_GSpbBJ59yG3lES264Z6yHXwtL8rhgjOEsu889KwE6xGToSnuQXGqWemZGEs4hBh8XRVebR8uXeQIUcgBpk0u3ypkYa_7Cy04DYUDWQG8JWy2DJMENJ73C0rEuPcS9yvXBzbsyCBNMngyOxeoEP4T5TD752ePs9TUPGRYgn7-VJFcr0UgwYTy0SRCYUlobxu0)
 wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann. Wird als *AUSGABEFORMAT* `uml` gewählt erhält man den bearbeitbaren Quelltext.

* Variante 2: **Editierbare Links** lassen sich auch über den Service von planttext.com erstellen, sie nutzen die selbe Quelltextcodierung und URLs nach dem Muster:
 [https://www.planttext.com/?text=CODIERTERQUELLTEXT](https://www.planttext.com/?text=bPBFJW8n4CRlVOg9Bs2ywhe199u8CS6OU2pjiDjijqEcKpQ4y6RUV35Ry028YVOuVxxVV5ywYg9PKkzLx5nOQzOzJ76bavTd2ZBNFSBDB1bdDInqYF2wNUF0Jf1lrCdEs8ZREDdk5EGtqQPhc5AmJ-I98GOQZWrYYtmiJZLt2wy59pxXeJjrkgTWBxURbg8CRMQUJVqgfVOdXyr9SFS7zYLqvffMtj7xVFd-p2ap3LUnwf2bkb-ODWSaSFS0AcGySD620wQgF1djNnXDzk34KQZhRriO4Tw8bsXTQ59ee4ynGhMiDyJLxR8-AenJN7r-j5m6RDbX67T51v1pmtk1Y2wen_pEa3d4wyovDkrFQCZLGlqN58E5OZb7GMi5EPDHBfNlzGK0)
 Auch in den erzeugten PNG-Dateien wird der codierte Quelltext als Titel hinterlegt, so dass sie sich relativ einfach später weiterverarbeiten lassen.

- Variante 3: **Quelltext direkt online rendern**: der PlantUML-Quelltext ist online als Resource verfügbar und soll gerendert ausgegeben werden. Hierzu muss eine URL nach dem Muster:
    [http://www.plantuml.com/plantuml/proxy?fmt=AUSGABEFORMAT&src=https://URL\_PLANTUMLSOURCE](http://www.plantuml.com/plantuml/proxy?fmt=epstxt&src=https://raw.githubusercontent.com/hannsens/plantUML-UseCase-InfoSheet/master/plantuml/01_Bestandteile_UseCase.plantuml)
    erstellt werden, wobei als *AUSGABEFORMAT* `png`, `svg`, `eps`, `epstext` und `txt` genutzt werden kann. Diese Funktion scheint derzeit deaktiviert zu sein oder nur Entwicklern vorbehalten.

### plantUML-Formatierung: Aufhübschen von UML-Diagrammen

Über Skins bietet PlantUML die Möglichkeit, die Diagramme in Schriftart, Farben und Aussehen individuell anzupassen.

Ich nutze für meine Diagramme derzeit die folgenden Einstellungen:

```plantuml
skinparam style strictuml

skinparam shadowing true

skinparam DefaultFontName "Lucida Sans Typewriter"

skinparam State{
    BackgroundColor snow
    BorderColor DarkSlateBlue
    DiamondBackgroundColor ghostwhite
    DiamondBorderColor DarkSlateBlue
}
skinparam Activity{
	DiamondBackgroundColor ghostwhite
	DiamondBorderColor DarkSlateBlue
}
skinparam Note{
    BorderColor DarkSlateBlue
    BackgroundColor LightYellow
}

skinparam ArrowColor DarkSlateBlue
skinparam lineType ortho
```

Codebeispiel: (
  [online bearbeitbar](https://www.planttext.com/?text=fPHFRwCm4CNFcKzXvPGS8cskg5lrJzHJrAsKlVJes9F46-EXzf1iLFMxrq0W41GkHG8PviTxCm-BXy3ISvvQ27lZCkbbAWAVBCQhD-ggii2Dp2s_aEDNcQ8OBtDbj1GhwOBuUyhmw0sZ7qDR3JzoT0h59uXuF7fFnsUovCL2-ltAIiOdcoEC7XMJVoAlqG_KfTflFULELynn5mr3Idi462tD1fV6uO18WmpflCL8Z3APT3mWrQPkgtsritWbnYg6sxfbzKflQROxVaThwTWLN4YMQncYw6YWXz5jjuvQ12mgDkJ2JmHtHgFO4F6Q_WEmuRYR_gm3-WDwyIOvxX07a5tY4sDb3JgUY-IvN8o53EByh0k6VFqljYYILRMeoD25w6nMw95J7Xbh_ugdEO2CWnvYrutFvK4inrFCxgDo8hMkNvuqmjCM7KhuxcusAzOQsvNsNM5Qv9a9IvI9oVpN5A0UlF8Ao2kEy2HGYkLg2bsdCBkFd2CsczEasmLKLPedUQOBaMblfFBbUeXI__UsjkoDL4pKmvl6hWPLeSHRvic1ZEu5K8EN6VfSQ58vXreXW2vURkTx_epm4BdYjmNV)
)

#### UML-Zustandsdiagramme per Formatierung als _Entwurf_ kennzeichnen

Um den Entwurfscharakter eines Diagramms zu unterstützen kann ein Layout verwendet werden, dass Handschrift ähnelt. Besonders wirkungsvoll ist das, wenn auch die Schrift authentisch handschriftlich aussieht. Dies ist z.B. bei der Schriftart "FG Virgil" der Fall (diese muss aber auf dem System installiert sein). Welche Schriften das jeweilige System bietet lässt sich mit dem PlantUML-Befehl `listfonts` anzeigen. Wichtig ist: sofern eine Vektorgrafik ausgegeben wird (svg, eps) muss die Schrift auf allen anzeigenden Computern vorhanden sein, andernfalls werden Ersatzschriftarten verwendet.

Die ensprechenden Skin-Parameter sind:

```plantuml
skinparam style strictuml
skinparam DefaultFontName "FG Virgil"
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam shadowing false
```

Das Ergebnis sieht dann etwa so aus:

![Entwurfscharakter durch Handschriftart hervorherben](plantuml/00_zustand-subzustaende6-handwritten.png)


#### Diagramme vereinheitlichen und Formatierung auslagern

Damit nicht alle PlantUML-Diagramme die selben `skinparam`-Sequenzen am Beginn der  plantUML-Datei nennen müssen, kann eine zentrale Konfigurationsdatei eingebunden werden, die die Formatierungen enthält:

```plantuml
!includeurl https://PATH.TO/MY/UMLSEQUENCE_CONFIG.cfg
```

Alle Befehle in dieser Datei werden ausgeführt, als stünden sie in der umgebenden PlantUML-Datei. Um ein einheitliches Layout zu erreichen ist diese Funktion sehr praktisch!

### Links

Es finden sich zahlreiche Quellen und Dokumentationen zu PlantUML im Netz, erwähnenswert sind u.a.:

- [Zuvorderst natürlich die offizielle Dokumentation von plantuml](https://plantuml.com/de/sequence-diagram)

- [Die PlantUML-Sprachreferenz als PDF](http://pdf.plantuml.net/PlantUML_Language_Reference_Guide_de.pdf)

- [diese Übersicht aller Skinparams ](https://plantuml-documentation.readthedocs.io/en/latest/diagrams/sequence.html)


